import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { debounceTime, switchMap, startWith } from "rxjs/operators";
import { of } from "rxjs";
import { MatTableModule, MatTable, MatTableDataSource } from '@angular/material/table';
import { CommonModule } from '@angular/common';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select'; 
 
 
export interface Item {
  id?: any;
  libelle?: any;
  pu?: any;
  quantity?: any;
  hors_tva?: any;
  tva_5_5?: any;
  tva_20?: any;
  cnipt?: any;
  ttc?: any;
}

export interface Commande {
  id? : string;
  date?: any; 
  client?: string;
  items: CmdItems;
}
export interface Article {
  id:any;
  libelle: string;
  pu: any;
}
type CmdItems = Array<Item>;

@Component({
  selector: 'app-commande',
  standalone: true,
  imports: [ReactiveFormsModule, 
    MatFormFieldModule, 
    MatCheckboxModule, 
    MatInputModule, 
    MatIconModule, 
    MatDividerModule, 
    MatListModule, 
    MatTableModule, 
    CommonModule, 
    MatSelectModule,
    // BrowserAnimationsModule
  ],
  templateUrl: './commande.component.html',
  styleUrl: './commande.component.css'
})
export class CommandeComponent implements OnInit {

  item?: Item;
  @ViewChild('current') current: ElementRef;

  @ViewChild('currentQTY') currentQTY: ElementRef; 
  @ViewChild('articleList') articleList: ElementRef; 

  dataSource = [];
  articles: Article[];
  currentCommande: Commande;
  displayedColumns: string[] = ['col-libelle', 'col-pu', 'col-quantity', 'col-ht', 'col-tva55', 'col-tva20', 'col-cnipt', 'col-ttc'];
 
  @ViewChild('CommandeItems', { static: true }) articleTable: MatTable<Item>;
  itemDataSource = new MatTableDataSource<Item>();
  serviceArticles = 'http://localhost:8081/articles/list'
  serviceCommand = 'http://localhost:8081/commandes/'

  constructor(private http: HttpClient) {
      
    const arr: CmdItems = []
    this.articles = []
    this.currentCommande = { client: "", date: "", items: arr }
  }
  ngOnInit() {
    this.http.get<Article[]>(this.serviceArticles)
      .subscribe((data: Article[]) => {
        // this.articleList.nativeElement.push({<option value="Vanilla"></option>})
        this.articles = data;
      });
  }
  totals() {
    let l = this.current.nativeElement.value
    let pu = this.getPU(l);
    let art_id = this.getArticleID(l);

    console.log(this.currentQTY);
    
    let quantity = this.currentQTY.nativeElement.value || 1
    this.currentCommande.items.push({
      id: art_id,
      libelle: l,
      pu: pu,
      quantity: quantity,
    })

    this.currentCommande.items.forEach(function (it, index, array) {
      it.hors_tva = it.pu * it.quantity;
      it.tva_5_5 = it.hors_tva * 0.055;
      it.tva_20 = it.hors_tva * 0.2;
      if (it.libelle?.includes('Pomme de terre')) {
        it.cnipt = it.hors_tva * 0.2;
      } else {
        it.cnipt = 0;
      }
      it.ttc = it.hors_tva + it.tva_5_5 + it.tva_20 + it.cnipt;
    });


    this.itemDataSource.data = this.currentCommande.items;

    console.log(this.currentCommande.items)
    this.current.nativeElement.value = ''
    this.articleTable.renderRows();
  }

  getArticleID(libelle: string){
    return this.articles.filter(x=> x.libelle==libelle).map(t => t.id).at(0);

  }
  getPU(libelle: string){
    return this.articles.filter(x=> x.libelle==libelle).map(t => t.pu).reduce((acc, value) => acc + value, 0);

  }

  getTotalTTC() {
    return this.currentCommande.items.map(t => t.ttc).reduce((acc, value) => acc + value, 0);
  }

  getTotalCNIPT() {
    return this.currentCommande.items.map(t => t.cnipt).reduce((acc, value) => acc + value, 0);
  }

  getTotalTVA55() {
    return this.currentCommande.items.map(t => t.tva_5_5).reduce((acc, value) => acc + value, 0);
  }

  getTotalTVA20() {
    return this.currentCommande.items.map(t => t.tva_20).reduce((acc, value) => acc + value, 0);
  }
  getTotalHT() {
    return this.currentCommande.items.map(t => t.hors_tva).reduce((acc, value) => acc + value, 0);
  }

  save() { 

    this.http.post<Commande[]>(this.serviceCommand+"save", this.currentCommande)
    .subscribe((data: any) => {
      this.currentCommande.id = data.id ;
      this.currentCommande.date = data.createDate ;

      console.log(data)

      this.http.get<Commande[]>(this.serviceCommand+data.id+"/item/list")
        .subscribe((data: any) => {
          this.currentCommande.items = data; 

          console.log(data)

        }); 

    }); 
  }

  search = new FormControl();
  articleControl = new FormControl();
  //   typesOfShoes: string[] = [
  //     "Boots",
  //     "Clogs",
  //     "Loafers",
  //     "Moccasins",
  //     "Sneakers"
  //   ];
  $search = this.search.valueChanges.pipe(
    startWith(null),
    debounceTime(200),
    switchMap((res: string) => {
      console.log(res)
      if (!res) return of(this.articles);
      res = res.toLowerCase();
      return of(
        this.articles.filter(x => x.libelle.toLowerCase().indexOf(res) >= 0)
      );
    })
  );
  selectionChange(option: any) {
    let value = this.articleControl.value || [];
    if (option.selected) value.push(option.value);
    else value = value.filter((x: any) => x != option.value);
    this.articleControl.setValue(value);
    // this.item.libelle =  option[0].value

    //     this.current.nativeElement.innerHTML = "huhu";
    this.current.nativeElement.value = option[0].value
    //     this.current.nativeElement.innerHTML = option[0]._value;
    //     console.log(option, this.current.nativeElement.innerHTML)

  }
}

