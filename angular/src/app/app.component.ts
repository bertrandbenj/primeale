import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { ArticleComponent } from './article/article.component';
import { CommandeComponent } from './commande/commande.component';
// import { TableComponent } from './table/table.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [ArticleComponent, CommandeComponent],
  template: `
      <main>
        <header class="brand-name" style="width:100%; text-align: center;">
          <img class="brand-logo" src="/assets/primeale.png" alt="logo" aria-hidden="true" width="400px">
        </header>
        <section class="content">
          <app-commande></app-commande>
          <app-article></app-article>
        </section>
      </main>
    `,
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'Priméale';
}
