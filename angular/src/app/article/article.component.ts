import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import {MatTableDataSource, MatSort} from '@angular/material';
import { MatTableModule, MatTable, MatTableDataSource } from '@angular/material/table';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button'; 


const ELEMENT_DATA: Article[] = [
  { id:1, libelle: 'article libelle', pu: 1 },
];

// @NgModule({
//   imports: [
//     HttpClientModule
//   ]
// })
@Component({
  selector: 'app-article',
  standalone: true,
  templateUrl: './article.component.html',
  imports: [ReactiveFormsModule, MatTableModule],
  styleUrl: './article.component.css'
})
export class ArticleComponent implements OnInit {
  newArticle: any;
  //   public tableDataSource : any ;
  displayedColumns: string[] = ['col-libelle', 'col-pu'];
  //   dataSource = [...ELEMENT_DATA];
  dataSource = new MatTableDataSource<Article>(ELEMENT_DATA);
  //
  //   @ViewChild(MatPaginator, {static: false}) matPaginator: MatPaginator;
  //   @ViewChild(MatSort, {static: true}) matSort: MatSort;


  @ViewChild('ArticleTable', { static: true }) articleTable: MatTable<Article>;


  serviceURL = 'http://localhost:8081/articles/list'

  constructor(private http: HttpClient) {
    this.dataSource = new MatTableDataSource<Article>(ELEMENT_DATA);
    this.dataSource.data = ELEMENT_DATA
  }

  ngOnInit() {
    this.http.get<Article[]>(this.serviceURL)
      .subscribe((data: Article[]) => {
        this.dataSource.data = data;
      });
  }

}

export interface Article {
  id : any;
  libelle: any;
  pu: any;
}
