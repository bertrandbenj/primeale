import { NgModule } from "@angular/core";
import { MatTableModule } from "@angular/material/table";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ArticleComponent } from "./article/article.component";
import { CommandeComponent } from "./commande/commande.component";
import { AppComponent } from "./app.component";
import { ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDividerModule } from "@angular/material/divider";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";
import { MatSelectModule } from "@angular/material/select";

@NgModule({
  imports: [
    MatTableModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule, 
    MatTableModule,
    MatFormFieldModule, 
    MatCheckboxModule, 
    MatInputModule, 
    MatIconModule, 
    MatDividerModule, 
    MatListModule, 
    MatTableModule, 
    CommonModule, 
    MatSelectModule,
  ],
  // exports: [ArticleComponent, CommandeComponent]
  // bootstrap: [AppComponent]

})

export class AppModule { }
