package com.example.primeale.service;

import com.example.primeale.dto.FullCommande;
import com.example.primeale.model.Article;
import com.example.primeale.model.Commande;
import com.example.primeale.model.CommandeItem;
import com.example.primeale.repo.ArticleRepository;
import com.example.primeale.repo.CommandeItemRepository;
import com.example.primeale.repo.CommandeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommandService {
    @Autowired
    CommandeRepository commandeRepository;

    @Autowired
    CommandeItemRepository itemsRepository;

    @Autowired
    ArticleRepository articleRepository;

    public List<CommandeItem> buildCommandes(Integer commandId) {
        var cmd = commandeRepository.findById(commandId).orElseThrow();

        for (Article article : articleRepository.findAll()) {
            var item = new CommandeItem();
            item.setCmd(cmd);
            item.setArticle(article);
            item.setPu(100.);
            item.setQuantity(1);
            item.totals();

            itemsRepository.saveAndFlush(item);
        }
        return itemsRepository.findAll();

    }

    public List<Commande> listCommandes() {
        return commandeRepository.findAll();
    }

    public List<CommandeItem> itemList(Integer commandId) {
        return itemsRepository.findByCommandId(commandId);
    }

    public Commande save(FullCommande c) {
        System.out.println("saving "+ c);

        var cmd = new Commande();
        if(c.getId()!=null && c.getId()!=0){
            cmd = commandeRepository.getReferenceById(c.getId());
        }

        cmd.setClient(c.getClient());

        var cres = commandeRepository.save(cmd);

        for (CommandeItem item : c.getItems()) {
            item.setCmd(cres);
            var art = articleRepository.getById(item.getId());
            item.setArticle(art);
            item.totals();
            itemsRepository.saveAndFlush(item);
        }

        return cres;
    }
}
