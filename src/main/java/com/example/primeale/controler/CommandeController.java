package com.example.primeale.controler;

import com.example.primeale.dto.FullCommande;
import com.example.primeale.model.Commande;
import com.example.primeale.model.CommandeItem;
import com.example.primeale.service.CommandService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.List;

@RestController
@RequestMapping("/commandes")
public class CommandeController {

    private static final Logger log = LogManager.getLogger(CommandeController.class);

    @Autowired
    CommandService cmdService;

    @GetMapping(path = "/list")
    public List<Commande> list() {
        return cmdService.listCommandes();
    }

    @PostMapping(path = "/save", consumes = "application/json", produces = "application/json")
    public Commande save(@RequestBody FullCommande cmd) {
        return cmdService.save(cmd);

    }

    @GetMapping(path = "{commandId}/item/list")
    public List<CommandeItem> itemList(@PathVariable("commandId") Integer commandId) {
        return cmdService.itemList(commandId);
    }


    @GetMapping(path = "{commandId}/item/test")
    public List<CommandeItem> test(@PathVariable("commandId") Integer commandId) {
        return cmdService.buildCommandes(commandId);
    }
}
