package com.example.primeale.controler;

import com.example.primeale.model.Article;
import com.example.primeale.repo.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/articles")
public class ArticleControler {

    @Autowired
    ArticleRepository articleRepository;

    @GetMapping("/")
    public String hi() {
        return "hello world";
    }

    @GetMapping("/list")
    public List<Article> list() {
        System.out.println("huhu");
        System.out.println(articleRepository.findAll());
        return articleRepository.findAll();
    }
}
