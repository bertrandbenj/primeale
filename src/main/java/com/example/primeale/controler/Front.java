package com.example.primeale.controler;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class Front {

    @GetMapping("/")
    public String hi() {
        return "hello world";
    }

}
