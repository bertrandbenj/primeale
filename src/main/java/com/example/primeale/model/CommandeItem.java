package com.example.primeale.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "items", indexes = {
//        @Index(name = "index_cmd",  columnList="cmd_id", unique = false),
//        @Index(name = "index_article", columnList="article_id", unique = false)
}
)
public class CommandeItem {
    @ManyToOne
    @JoinColumn(name = "cmd_id")
    Commande cmd;

    @ManyToOne
    @JoinColumn(name = "article_id")
    Article article;
    @Column(name = "pu", nullable = false)
    Double pu;
    @Column(name = "qty", nullable = false)
    Integer quantity;
    /*
    totals
     */
    @Column(name = "hors_tva", nullable = false)
    Double hors_tva;
    @Column(name = "tva_5_5", nullable = false)
    Double tva_5_5;
    @Column(name = "tva_20", nullable = false)
    Double tva_20;
    @Column(name = "CNIPT", nullable = false)
    Double CNIPT;
    @Column(name = "TTC", nullable = false)
    Double TTC;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    /**
     * Règles sur les cotisations et TVA
     * Toutes les articles sont soumis à une « TVA marchandise » de valeur 5,5%
     * Une cotisation appelée « CNIPT » est appliquée en supplément de la « TVA marchandise » sur tous les articles contenant de la pomme de terre.
     * La cotisation « CNIPT » est soumise à une « TVA pomme de terre » de valeur 20 %
     */
    public void totals() {
        hors_tva = pu * quantity;
        tva_5_5 = hors_tva * 0.055;
        tva_20 = hors_tva * 0.2;
        CNIPT = article.libelle.contains("Pomme de terre") ? hors_tva * 0.2 : 0;

        TTC = hors_tva + tva_5_5 + tva_20 + CNIPT;
    }
}
