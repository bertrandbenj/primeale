package com.example.primeale.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Entity
@Table(name = "article")
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    Integer id;

    @Setter
    @Column(name = "libelle", nullable = false)
    String libelle;

    @Setter
    @Column(name = "pu", nullable = false)
    Double pu;


}