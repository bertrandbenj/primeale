package com.example.primeale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class PrimealeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrimealeApplication.class, args);
    }

}
