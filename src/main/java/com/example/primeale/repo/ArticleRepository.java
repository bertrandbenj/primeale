package com.example.primeale.repo;

import com.example.primeale.model.Article;
import com.example.primeale.model.CommandeItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ArticleRepository extends JpaRepository<Article, Integer> {

    @Query(value = "SELECT * FROM article WHERE id = ?1", nativeQuery = true)
    Article getById(Integer id);

}