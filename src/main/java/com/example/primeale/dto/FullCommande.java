package com.example.primeale.dto;

import com.example.primeale.model.CommandeItem;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class FullCommande {
    Integer id;
    String client;
    LocalDateTime date;
    List<CommandeItem> items;
}
